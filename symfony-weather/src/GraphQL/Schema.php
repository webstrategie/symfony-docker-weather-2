<?php

namespace App\GraphQL;

use Youshido\GraphQL\Schema\AbstractSchema;
use Youshido\GraphQL\Config\Schema\SchemaConfig;
use Youshido\GraphQL\Type\Object\ObjectType;
use Youshido\GraphQL\Type\Scalar\StringType;
use App\GraphQL\Types\Weather\WeatherType;
use App\GraphQL\Types\Weather\Fields\WeatherField;

class Schema extends AbstractSchema
{
    public function build(SchemaConfig $config)
    {
        $config->getQuery()->addFields([
            new WeatherField()
        ]);
    }
}
