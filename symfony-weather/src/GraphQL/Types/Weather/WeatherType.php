<?php
namespace App\GraphQL\Types\Weather;

use Youshido\GraphQL\Config\Object\ObjectTypeConfig;
use Youshido\GraphQL\Type\ListType\ListType;
use Youshido\GraphQL\Type\NonNullType;
use Youshido\GraphQL\Type\Object\AbstractObjectType;
use Youshido\GraphQL\Type\Scalar\BooleanType;
use Youshido\GraphQL\Type\Scalar\IdType;
use Youshido\GraphQL\Type\Scalar\IntType;
use Youshido\GraphQL\Type\Scalar\StringType;
use Youshido\GraphQL\Type\Scalar\FloatType;

class WeatherType extends AbstractObjectType
{
    public function build($config)
    {
        $config->addFields([
            'city'=> [
                'type'=> new StringType(),
                'decription'=>'Unique City of Weather',
                'args'=>[
                    'city' => new StringType()
                ],
            ],
            'wind_speed' => new FloatType(),
            'wind_direction' => new IntType(),
            'location' => new StringType(),
            'humidity' => new IntType(),
            'temperature' => new IntType(),
            'pressure' => new FloatType(),
        ]);
    }
}
