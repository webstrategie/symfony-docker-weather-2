<?php

namespace App\Service;

use App\Entity\Weather;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Handler\CurlHandler;
use GuzzleHttp\Subscriber\Oauth\Oauth1;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class WeatherConfiguration
{
    const DEFAULT_BASE_URI = 'https://weather-ydn-yql.media.yahoo.com/forecastrss';
    
    /**
     * @var string
     */
    private $baseUri;

    /**
     * @var string
     */
    private $consumerKey;

    /**
     * HTTP Client
     */
    private $Client;

    /**
     * @var array
     */
    private $consumerSecret;

    /**
     * @var string
     */
    private $apiKey;


    /**
     * Configuration constructor.
     * @param array $configuration
     */
    public function __construct(ParameterBagInterface $params)
    {
        $baseUri = $params->get('base_uri');
        $this->baseUri = (isset($baseUri))?$baseUri:self::DEFAULT_BASE_URI;
        $this->consumerKey = $params->get('consumer_key');
        $this->consumerSecret = $params->get('consumer_secret');
        $this->apiKey = $params->get('app_key');
        $this->client = $this->getHttpClient();
    }


    /**
     * @return string
     */
    public function baseUri()
    {
        return $this->baseUri;
    }

    /**
     * @param string $baseUri
     */
    public function setBaseUri($baseUri)
    {
        $this->baseUri = $baseUri;
    }

    /**
     * @return string
     */
    public function consumerKey()
    {
        return $this->consumerKey;
    }

    /**
     * @param string $consumerKey
     */
    public function setConsumerKey($consumerKey)
    {
        $this->consumerKey = $consumerKey;
    }

    /**
     * @return string
     */
    public function consumerSecret()
    {
        return $this->consumerSecret;
    }

    /**
     * @param string $consumerSecret
     */
    public function setConsumerSecret($consumerSecret)
    {
        $this->consumerSecret = $consumerSecret;
    }

    /**
     * @return string
     */
    public function apiKey()
    {
        return $this->apiKey;
    }

    /**
     * @return Client
     */
    public function getHttpClient()
    {
        if ($this->Client) {
            return $this->Client;
        }
        return $this->Client = $this->setDefaultHttpClient();
    }

    /**
     * @return Client
     */
    private function setDefaultHttpClient()
    {
        $stack = HandlerStack::create();

        $header = array( "X-Yahoo-App-Id" =>  $this->apiKey);


        $middleware = new Oauth1([
            'consumer_key' => $this->consumerKey,
            'consumer_secret' => $this->consumerSecret,
            'nonce' => uniqid(mt_rand(1, 1000)),
            'signature_method' => 'HMAC-SHA1',
            'timestamp' => time(),
            'version' => '1.0',
            'token' => null,
            'token_secret' => null,
            $header
        ]);

        $stack->push($middleware);

        return new Client([
            'handler' => $stack
        ]);
    }
}
